package com.mobilejazz.android.toolkit;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import com.mobilejazz.helpers.images.ImageLoader;
import com.mobilejazz.helpers.images.PicassoImageLoader;
import com.mobilejazz.helpers.images.model.TransformationOptions;
import com.mobilejazz.helpers.images.model.TransformationOptionsBlur;
import com.mobilejazz.helpers.location.LocationProvider;
import com.squareup.picasso.Picasso;

public class MainActivity extends AppCompatActivity {

  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ImageView imageViewOne = (ImageView) findViewById(R.id.imageView_one);
    ImageView imageViewTwo = (ImageView) findViewById(R.id.imageView_two);

    Picasso.Builder builder = new Picasso.Builder(this);
    builder.indicatorsEnabled(true);
    builder.loggingEnabled(true);

    ImageLoader imageLoader = new PicassoImageLoader(this, builder.build());

    imageLoader.load("http://www.stockvault.net/blog/wp-content/uploads/2013/11/Portrait-8.jpg",
        PicassoImageLoader.EMPTY_PLACEHOLDER, imageViewOne, ImageLoader.Transformation.CIRCLE,
        new TransformationOptions.EmptyTransformationOptions());

    imageLoader.load("http://www.dannyst.com/blogimg/gallery-portraits-of-strangers-18.jpg",
        PicassoImageLoader.EMPTY_PLACEHOLDER, imageViewTwo, ImageLoader.Transformation.BLUR,
        new TransformationOptionsBlur(20));

    obtainLocation();
  }

  private void obtainLocation() {
    LocationProvider provider = new LocationProvider(this);

    provider.getLocation(LocationProvider.Method.NETWORK_THEN_GPS, new LocationProvider.Listener() {
      @Override public void onLocationFound(Location location) {
        Log.d("TMP_LOG", "MainActivity.onLocationFound(): ");
      }

      @Override public void onLocationNotFound() {
        Log.d("TMP_LOG", "MainActivity.onLocationNotFound(): ");
      }

      @Override public void onPermissionDenied() {
        Log.d("TMP_LOG", "MainActivity.onPermissionDenied(): ");
      }

      @Override public void onPermissionRationaleShouldBeShown() {
        Log.d("TMP_LOG", "MainActivity.onPermissionRationaleShouldBeShown(): ");
      }
    });
  }
}
