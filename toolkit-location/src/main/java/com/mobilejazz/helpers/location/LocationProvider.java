package com.mobilejazz.helpers.location;

import android.Manifest;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.PermissionChecker;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

/**
 * Get device location using various methods
 *
 * @author emil http://stackoverflow.com/users/220710/emil
 */
public class LocationProvider implements LocationListener {

  static private final String LOG_TAG = LocationProvider.class.getSimpleName();

  static private final int TIME_INTERVAL = 100; // minimum time between updates in milliseconds
  static private final int DISTANCE_INTERVAL = 1; // minimum distance between updates in meters

  public enum Method {
    NETWORK,
    GPS,
    NETWORK_THEN_GPS
  }

  private Context context;
  private LocationManager locationManager;
  private LocationProvider.Method method;
  private LocationProvider.Listener listener;

  public LocationProvider(Context context) {
    super();
    this.context = context;
    this.locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
    Dexter.initialize(context);
  }

  public void getLocation(LocationProvider.Method method,
      final LocationProvider.Listener listener) {
    this.method = method;
    this.listener = listener;
    switch (this.method) {
      case NETWORK:
      case NETWORK_THEN_GPS:
        if (Dexter.isRequestOngoing()) {
          return;
        }
        Dexter.checkPermissionOnSameThread(new PermissionListener() {
          @Override public void onPermissionGranted(PermissionGrantedResponse response) {
            Location networkLocation = LocationProvider.this.locationManager.getLastKnownLocation(
                LocationManager.NETWORK_PROVIDER);

            if (networkLocation != null) {
              LocationProvider.this.listener.onLocationFound(networkLocation);
            } else {
              LocationProvider.this.requestUpdates(LocationManager.NETWORK_PROVIDER);
            }
          }

          @Override public void onPermissionDenied(PermissionDeniedResponse response) {
            listener.onPermissionDenied();
          }

          @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
              PermissionToken token) {
            listener.onPermissionRationaleShouldBeShown();
            token.continuePermissionRequest();
          }
        }, Manifest.permission.ACCESS_FINE_LOCATION);

        break;
      case GPS:
        Location gpsLocation =
            this.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if (gpsLocation != null) {
          this.listener.onLocationFound(gpsLocation);
        } else {
          this.requestUpdates(LocationManager.GPS_PROVIDER);
        }
        break;
    }
  }

  private void requestUpdates(final String provider) {
    if (this.locationManager.isProviderEnabled(provider)) {
      if (provider.contentEquals(LocationManager.NETWORK_PROVIDER) && Connectivity.isConnected(
          this.context)) {
        requestLocationUpdateWithPermission(provider);
      } else if (provider.contentEquals(LocationManager.GPS_PROVIDER)
          && Connectivity.isConnectedMobile(this.context)) {
        requestLocationUpdateWithPermission(provider);
      } else {
        this.onProviderDisabled(provider);
      }
    } else {
      this.onProviderDisabled(provider);
    }
  }

  private void requestLocationUpdateWithPermission(final String provider) {
    if (Dexter.isRequestOngoing()) {
      return;
    }
    Dexter.checkPermissionOnSameThread(new PermissionListener() {
      @Override public void onPermissionGranted(PermissionGrantedResponse response) {
        LocationProvider.this.locationManager.requestLocationUpdates(provider, TIME_INTERVAL,
            DISTANCE_INTERVAL, LocationProvider.this);
      }

      @Override public void onPermissionDenied(PermissionDeniedResponse response) {
        listener.onPermissionDenied();
      }

      @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
          PermissionToken token) {
        listener.onPermissionRationaleShouldBeShown();
        token.continuePermissionRequest();
      }
    }, Manifest.permission.ACCESS_FINE_LOCATION);
  }

  public void cancel() {
    if (Dexter.isRequestOngoing()) {
      return;
    }
    Dexter.checkPermissionOnSameThread(new PermissionListener() {
      @Override public void onPermissionGranted(PermissionGrantedResponse response) {
        LocationProvider.this.locationManager.removeUpdates(LocationProvider.this);
      }

      @Override public void onPermissionDenied(PermissionDeniedResponse response) {
        listener.onPermissionDenied();
      }

      @Override public void onPermissionRationaleShouldBeShown(PermissionRequest permission,
          PermissionToken token) {
        listener.onPermissionRationaleShouldBeShown();
        token.continuePermissionRequest();
      }
    }, Manifest.permission.ACCESS_FINE_LOCATION);
  }

  @Override public void onLocationChanged(Location location) {
    cancel();
    this.listener.onLocationFound(location);
  }

  @Override public void onProviderDisabled(String provider) {
    if (this.method == LocationProvider.Method.NETWORK_THEN_GPS && provider.contentEquals(
        LocationManager.NETWORK_PROVIDER)) {
      // Network provider disabled, try GPS
      this.requestUpdates(LocationManager.GPS_PROVIDER);
    } else {
      cancel();
      this.listener.onLocationNotFound();
    }
  }

  @Override public void onProviderEnabled(String provider) {
  }

  @Override public void onStatusChanged(String provider, int status, Bundle extras) {
  }

  public interface Listener {

    void onLocationFound(Location location);

    void onLocationNotFound();

    void onPermissionDenied();

    void onPermissionRationaleShouldBeShown();
  }
}
