package com.mobilejazz.helpers.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

/**
 * Util class for perfoming common operations related to fragments.
 */
public class Fragments {

  public static void replace(FragmentActivity activity, Fragment f, @IdRes int container) {
    replace(activity.getSupportFragmentManager(), f, container);
  }

  public static void replace(FragmentActivity activity, Fragment f, @IdRes int container,
      String tag) {
    replace(activity.getSupportFragmentManager(), f, container, tag);
  }

  public static void replace(FragmentManager manager, Fragment f, @IdRes int container) {
    manager.beginTransaction().replace(container, f).commit();
  }

  public static void replace(FragmentManager manager, Fragment f, @IdRes int container, String tag) {
    manager.beginTransaction().replace(container, f, tag).commit();
  }

  @Nullable public static Fragment findByTag(FragmentActivity activity, String tag) {
    return activity.getSupportFragmentManager().findFragmentByTag(tag);
  }

  @Nullable public static Fragment findyById(FragmentActivity activity, int id) {
    return activity.getSupportFragmentManager().findFragmentById(id);
  }

  public static Bundle intentToFragmentArguments(Intent intent) {
    Bundle arguments = new Bundle();
    if (intent == null) {
      return arguments;
    }

    final Uri data = intent.getData();
    if (data != null) {
      arguments.putParcelable("_uri", data);
    }

    final Bundle extras = intent.getExtras();
    if (extras != null) {
      arguments.putAll(intent.getExtras());
    }

    return arguments;
  }

  public static Intent fragmentArgumentsToIntent(Bundle arguments) {
    Intent intent = new Intent();
    if (arguments == null) {
      return intent;
    }

    final Uri data = arguments.getParcelable("_uri");
    if (data != null) {
      intent.setData(data);
    }

    intent.putExtras(arguments);
    intent.removeExtra("_uri");
    return intent;
  }

  private Fragments() {
    throw new AssertionError("No instances of this class are allowed!");
  }
}