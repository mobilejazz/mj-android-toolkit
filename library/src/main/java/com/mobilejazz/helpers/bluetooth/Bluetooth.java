package com.mobilejazz.helpers.bluetooth;

import android.bluetooth.BluetoothAdapter;

public class Bluetooth {

  public static boolean isBluetoothEnabled() {
    return BluetoothAdapter.getDefaultAdapter() != null && BluetoothAdapter.getDefaultAdapter()
        .isEnabled();
  }

  private Bluetooth() {
    throw new AssertionError("No instances of this class are allowed!");
  }
}
