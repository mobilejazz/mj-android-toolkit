package com.mobilejazz.helpers.images;

import android.content.Context;
import android.widget.ImageView;
import com.mobilejazz.helpers.images.model.TransformationOptions;
import com.mobilejazz.helpers.images.model.TransformationOptionsBlur;
import com.mobilejazz.helpers.images.transformations.BlurTransform;
import com.mobilejazz.helpers.images.transformations.CircleTransform;
import com.squareup.picasso.Picasso;
import java.io.File;
import javax.inject.Inject;

public class PicassoImageLoader implements ImageLoader {

  public static final int EMPTY_PLACEHOLDER = -1;

  private Context context;
  private Picasso picasso;

  @Inject public PicassoImageLoader(Context context, Picasso picasso) {
    this.context = context.getApplicationContext();
    this.picasso = picasso;
  }

  @Override public void load(String url, int resPlaceholderIcon, ImageView imageView,
      Transformation transformation, TransformationOptions option) {
    switch (transformation) {
      case CIRCLE:
        loadCircleImage(url, resPlaceholderIcon, imageView);
        break;
      case BLUR:
        TransformationOptionsBlur blurOption = (TransformationOptionsBlur) option;
        loadBlurImage(url, resPlaceholderIcon, imageView, blurOption);
        break;
      case NONE:
        load(url, resPlaceholderIcon, imageView);
        break;
      default:
        throw new IllegalArgumentException("Transformation transformation must be define");
    }
  }

  @Override public void load(int resIcon, int resPlaceholderIcon, ImageView imageView,
      Transformation transformation, TransformationOptions option) {
    switch (transformation) {
      case CIRCLE:
        loadCircleImage(resIcon, resPlaceholderIcon, imageView);
        break;
      case BLUR:
        TransformationOptionsBlur blurOption = (TransformationOptionsBlur) option;
        loadBlurImage(resIcon, resPlaceholderIcon, imageView, blurOption);
        break;
      case NONE:
        load(resIcon, resPlaceholderIcon, imageView);
        break;
      default:
        throw new IllegalArgumentException("Transformation transformation must be define");
    }
  }

  @Override public void load(File file, int resPlaceholderIcon, ImageView imageView,
      Transformation transformation, TransformationOptions option) {
    switch (transformation) {
      case CIRCLE:
        loadCircleImage(file, resPlaceholderIcon, imageView);
        break;
      case BLUR:
        TransformationOptionsBlur blurOption = (TransformationOptionsBlur) option;
        loadBlurImage(file, resPlaceholderIcon, imageView, blurOption);
        break;
      case NONE:
        load(file, resPlaceholderIcon, imageView);
        break;
      default:
        throw new IllegalArgumentException("Transformation transformation must be define");
    }
  }

  //region Private methods

  private void load(String url, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(url).placeholder(resPlaceholderIcon).into(imageView);
    } else {
      picasso.load(url).into(imageView);
    }
  }

  private void load(int resIcon, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(resIcon).placeholder(resPlaceholderIcon).into(imageView);
    } else {
      picasso.load(resIcon).into(imageView);
    }
  }

  private void load(File file, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(file).placeholder(resPlaceholderIcon).into(imageView);
    } else {
      picasso.load(file).into(imageView);
    }
  }

  private void loadCircleImage(String url, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(url)
          .placeholder(resPlaceholderIcon)
          .transform(new CircleTransform())
          .into(imageView);
    } else {
      picasso.load(url).transform(new CircleTransform()).into(imageView);
    }
  }

  private void loadCircleImage(File file, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(file)
          .placeholder(resPlaceholderIcon)
          .transform(new CircleTransform())
          .into(imageView);
    } else {
      picasso.load(file).transform(new CircleTransform()).into(imageView);
    }
  }

  private void loadCircleImage(int resIcon, int resPlaceholderIcon, ImageView imageView) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(resIcon)
          .placeholder(resPlaceholderIcon)
          .transform(new CircleTransform())
          .into(imageView);
    } else {
      picasso.load(resIcon).transform(new CircleTransform()).into(imageView);
    }
  }

  private void loadBlurImage(String url, int resPlaceholderIcon, ImageView imageView,
      TransformationOptionsBlur options) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(url)
          .placeholder(resPlaceholderIcon)
          .transform(new BlurTransform(context, options.getRadius()))
          .into(imageView);
    } else {
      picasso.load(url).transform(new BlurTransform(context, options.getRadius())).into(imageView);
    }
  }

  private void loadBlurImage(File file, int resPlaceholderIcon, ImageView imageView,
      TransformationOptionsBlur options) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(file)
          .placeholder(resPlaceholderIcon)
          .transform(new BlurTransform(context, options.getRadius()))
          .into(imageView);
    } else {
      picasso.load(file).transform(new BlurTransform(context, options.getRadius())).into(imageView);
    }
  }

  private void loadBlurImage(int resIcon, int resPlaceholderIcon, ImageView imageView,
      TransformationOptionsBlur options) {
    if (hasPlaceHolder(resPlaceholderIcon)) {
      picasso.load(resIcon)
          .placeholder(resPlaceholderIcon)
          .transform(new BlurTransform(context, options.getRadius()))
          .into(imageView);
    } else {
      picasso.load(resIcon)
          .transform(new BlurTransform(context, options.getRadius()))
          .into(imageView);
    }
  }

  private boolean hasPlaceHolder(int resPlaceholderIcon) {
    return resPlaceholderIcon != EMPTY_PLACEHOLDER;
  }

  //endregion
}
