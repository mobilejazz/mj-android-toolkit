package com.mobilejazz.helpers.images.model;

public class TransformationOptionsBlur implements TransformationOptions {
  private int radius;

  public TransformationOptionsBlur(int radius) {
    this.radius = radius;
  }

  public int getRadius() {
    return radius;
  }

  public void setRadius(int radius) {
    this.radius = radius;
  }
}
