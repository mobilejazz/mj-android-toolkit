package com.mobilejazz.helpers.images;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.v4.util.Pair;
import android.util.Log;
import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import com.mobilejazz.helpers.files.Files;

public class Bitmaps {

  public static final String TAG = Bitmaps.class.getSimpleName();

  public static final int DEFAULT_THUMBNAIL_SIZE = 128; // In px

  public static Bitmap getThumbnail(String path) {
    return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeFile(path), DEFAULT_THUMBNAIL_SIZE,
        DEFAULT_THUMBNAIL_SIZE);
  }

  public static Bitmap getThumbnail(ContentResolver cr, Uri uri) throws FileNotFoundException {
    return ThumbnailUtils.extractThumbnail(BitmapFactory.decodeStream(cr.openInputStream(uri)),
        DEFAULT_THUMBNAIL_SIZE, DEFAULT_THUMBNAIL_SIZE);
  }

  private Bitmaps() {
    throw new AssertionError("Class not intended for instantiation!");
  }

  public static class ThumbnailTask extends AsyncTask<Void, Void, Pair<File, Bitmap>> {

    private WeakReference<Context> c;
    private Uri uri;

    public ThumbnailTask(Context c, Uri uri) {
      this.c = new WeakReference<>(c);
      this.uri = uri;
    }

    @Override protected Pair<File, Bitmap> doInBackground(Void... parameters) {
      Context context = c.get();

      if (c != null) {
        String filePath =
            Files.getRealPathFromUri(context, uri, new String[] { MediaStore.Images.Media.DATA },
                MediaStore.Images.Media.DATA);
        Bitmap bitmap = null;
        try {
          bitmap = Bitmaps.getThumbnail(context.getContentResolver(), uri);
        } catch (FileNotFoundException e) {
          Log.e(TAG, "Bitmap file not found!");
        }

        return new Pair<>(new File(filePath), bitmap);
      }

      return null;
    }
  }

  public static class LoadThumbnailTask extends AsyncTask<Void, Void, Bitmap> {

    private final String path;

    public LoadThumbnailTask(String path) {
      this.path = path;
    }

    @Override protected Bitmap doInBackground(Void... params) {
      return Bitmaps.getThumbnail(path);
    }
  }
}
