package com.mobilejazz.helpers.images;

import android.widget.ImageView;
import com.mobilejazz.helpers.images.model.TransformationOptions;
import java.io.File;

public interface ImageLoader {

  enum Transformation {
    CIRCLE,
    BLUR,
    NONE
  }

  void load(String url, int resPlaceholderIcon, ImageView imageView, Transformation transformation,
      TransformationOptions option);

  void load(int resIcon, int resPlaceholderIcon, ImageView imageView, Transformation transformation,
      TransformationOptions option);

  void load(File file, int resPlaceholderIcon, ImageView imageView, Transformation transformation,
      TransformationOptions option);
}
