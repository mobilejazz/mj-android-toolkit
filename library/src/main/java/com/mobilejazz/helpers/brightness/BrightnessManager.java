package com.mobilejazz.helpers.brightness;

import android.content.Context;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

public class BrightnessManager {

  public enum Mode {
    AUTOMATIC(Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC),
    MANUAL(Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL),
    UNKNOWN(-1); // Probably, the screen does not support brightness?

    private int mode;

    Mode(int mode) {
      this.mode = mode;
    }

    int getIntMode() {
      return this.mode;
    }
  }

  private final Context context;

  public BrightnessManager(Context context) {
    this.context = context.getApplicationContext();
  }

  public Mode getBrightnessMode() {
    int mode = Settings.System.getInt(this.context.getContentResolver(),
        Settings.System.SCREEN_BRIGHTNESS_MODE, -1);

    switch (mode) {
      case Settings.System.SCREEN_BRIGHTNESS_MODE_MANUAL:
        return Mode.MANUAL;
      case Settings.System.SCREEN_BRIGHTNESS_MODE_AUTOMATIC:
        return Mode.AUTOMATIC;
      default:
        return Mode.UNKNOWN;
    }
  }

  public void setBrightnessMode(Mode mode) {
    Settings.System.putInt(this.context.getContentResolver(),
        Settings.System.SCREEN_BRIGHTNESS_MODE, mode.getIntMode());
  }

  public void setBrightness(int level) {
    setBrightness(null, level);
  }

  public void setBrightness(Window window, int level) {
    Mode brightnessMode = getBrightnessMode();

    switch (brightnessMode) {
      case AUTOMATIC:
        setBrightnessForAutomaticMode(window, level);
        break;
      case MANUAL:
        setBrightnessForManualMode(window, level);
        break;
      case UNKNOWN:
      default:
        break;
    }
  }

  public int getBrightnessValue() {
    return Settings.System.getInt(this.context.getContentResolver(),
        Settings.System.SCREEN_BRIGHTNESS, 0);
  }

  private void setBrightnessForAutomaticMode(Window window, int level) {
    //Settings.System.putInt(this.context.getContentResolver(), );
    //setBrightnessForWindow(window, level);
  }

  private void setBrightnessForManualMode(Window window, int level) {
    Settings.System.putInt(this.context.getContentResolver(), Settings.System.SCREEN_BRIGHTNESS,
        level);
    setBrightnessForWindow(window, level);
  }

  private void setBrightnessForWindow(Window window, int level) {
    if (window != null) {
      WindowManager.LayoutParams lp = window.getAttributes();
      lp.screenBrightness = (float) level / 100f;
      window.setAttributes(lp);
    }
  }
}
