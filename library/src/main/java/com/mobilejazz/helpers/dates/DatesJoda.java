package com.mobilejazz.helpers.dates;

import java.util.Date;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class DatesJoda implements Dates {

  @Inject public DatesJoda() {
  }

  @Override public Date addMinutes(Date date, int minutes) {
    DateTime dateIncrementDays = asDateTime(date).plusMinutes(minutes);
    return asDate(dateIncrementDays);
  }

  @Override public Date minusMinutes(Date date, int minutes) {
    DateTime dateIncrementDays = asDateTime(date).minusMinutes(minutes);
    return asDate(dateIncrementDays);
  }

  @Override public Date addDays(Date date, int days) {
    DateTime dateIncrementDays = asDateTime(date).plusDays(days);
    return asDate(dateIncrementDays);
  }

  @Override public Date minusDays(Date date, int days) {
    DateTime dateIncrementDays = asDateTime(date).minusDays(days);
    return asDate(dateIncrementDays);
  }

  @Override public Date today() {
    return new Date();
  }

  @Override public Date addMonths(Date date, int months) {
    DateTime dateIncrementDays = asDateTime(date).plusMonths(months);
    return asDate(dateIncrementDays);
  }

  @Override public Date minusMonths(Date date, int months) {
    DateTime dateIncrementDays = asDateTime(date).minusDays(months);
    return asDate(dateIncrementDays);
  }

  @Override public Date now() {
    return new Date();
  }

  @Override public boolean isToday(Date date) {
    return ((new DateTime(date).toLocalDate()).equals(new LocalDate()));
  }

  @Override public int daysBetween(Date dateOne, Date dateTwo) {
    return Days.daysBetween(asDateTime(dateOne), asDateTime(dateTwo)).getDays();
  }

  @Override public String format(Date date, String format) {
    DateTime dateTime = new DateTime(date);
    DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
    return dateTime.toString(formatter);
  }

  @Override public String format(Date date, DateFormatterStyle style) {
    DateTime dateTime = new DateTime(date);

    DateTimeFormatter formatter;
    switch (style) {
      case SHORT:
        formatter = DateTimeFormat.shortDate();
        break;
      case MEDIUM:
        formatter = DateTimeFormat.mediumDate();
        break;
      case LONG:
        formatter = DateTimeFormat.longDate();
        break;
      default:
        formatter = DateTimeFormat.mediumDate();
        break;
    }

    return dateTime.toString(formatter);
  }

  ///////////////////////////////////////////////////////////////////////////
  // Private methods
  ///////////////////////////////////////////////////////////////////////////

  private DateTime asDateTime(Date date) {
    return date == null ? null : new DateTime(date);
  }

  private Date asDate(DateTime dateTime) {
    return dateTime == null ? null : dateTime.toDate();
  }
}
