package com.mobilejazz.helpers.dates;

import java.util.Date;

public interface Dates {

  enum DateFormatterStyle {
    SHORT,
    MEDIUM,
    LONG
  }

  Date addMinutes(Date date, int minutes);

  Date minusMinutes(Date date, int minutes);

  Date addDays(Date date, int days);

  Date minusDays(Date date, int days);

  Date addMonths(Date date, int months);

  Date minusMonths(Date date, int months);

  Date today();

  Date now();

  boolean isToday(Date date);

  int daysBetween(Date dateOne, Date dateTwo);

  String format(Date date, String format);

  String format(Date date, DateFormatterStyle style);
}
