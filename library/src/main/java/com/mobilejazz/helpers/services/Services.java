package com.mobilejazz.helpers.services;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;

public final class Services {

  public static <T extends Service> boolean isServiceRunning(Context context, Class<T> serviceClass) {
    final ActivityManager manager =
        (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
      if (serviceClass.getName().equals(service.service.getClassName())) {
        return true;
      }
    }
    return false;
  }

  private Services() {
    throw new AssertionError("Services class is not intended for creating instances!");
  }

}
