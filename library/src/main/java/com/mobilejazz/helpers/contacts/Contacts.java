package com.mobilejazz.helpers.contacts;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import java.lang.ref.WeakReference;

public class Contacts {

  public interface CompletionCallback<T> {
    void onContactObtained(T contact);

    void onContactNotFound();
  }

  public interface ContactMapper<T> {
    T transform(Cursor cursor);
  }

  private Contacts() {
    throw new AssertionError("Class not intended for instantiation!");
  }

  public static <T> void obtainContact(Intent data, ContentResolver cr, final ContactMapper<T> mapper,
      final CompletionCallback<T> callback) {
    ContactAsyncHandler.AsyncQueryListener listener = new ContactAsyncHandler.AsyncQueryListener() {
      @Override public void onQueryComplete(int token, Object cookie, Cursor cursor) {

        if (cursor.moveToFirst()) {
          if (callback != null) {
            callback.onContactObtained(mapper.transform(cursor));
          }
          if (!cursor.isClosed()) {
            cursor.close();
          }
        } else {
          if (callback != null) {
            callback.onContactNotFound();
          }
          if (!cursor.isClosed()) {
            cursor.close();
          }
        }
      }
    };
    new ContactAsyncHandler(cr, listener).startQuery(1, null, data.getData(), null, null, null,
        null);
  }

  public static class ContactAsyncHandler extends AsyncQueryHandler {

    public interface AsyncQueryListener {
      void onQueryComplete(int token, Object cookie, Cursor cursor);
    }

    private WeakReference<AsyncQueryListener> listener;

    public ContactAsyncHandler(ContentResolver cr, AsyncQueryListener listener) {
      super(cr);
      setQueryListener(listener);
    }

    public void setQueryListener(AsyncQueryListener listener) {
      this.listener = new WeakReference<>(listener);
    }

    @Override protected void onQueryComplete(int token, Object cookie, Cursor cursor) {
      final AsyncQueryListener listener = this.listener.get();
      if (listener != null) {
        listener.onQueryComplete(token, cookie, cursor);
      } else if (cursor != null) {
        cursor.close();
      }
    }
  }
}
