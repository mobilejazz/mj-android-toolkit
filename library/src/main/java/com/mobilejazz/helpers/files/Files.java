package com.mobilejazz.helpers.files;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class Files {

  public static String getRealPathFromUri(Context context, Uri uri, String[] projection,
      String column) {
    String path = null;
    Cursor c = context.getContentResolver().query(uri, projection, null, null, null);
    if (c != null) {
      int columnIndex = -1;
      try {
        columnIndex = c.getColumnIndexOrThrow(column);
      } catch (IllegalArgumentException e) {
        // Ignored, checked below
      }
      boolean moved = columnIndex >= 0 && c.moveToFirst();
      if (moved) {
        path = c.getString(columnIndex);
      }
      c.close();
    }
    return path;
  }

  private Files() {
    throw new AssertionError("Class not intended for instantiation!");
  }
}
