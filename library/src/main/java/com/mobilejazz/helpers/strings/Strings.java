package com.mobilejazz.helpers.strings;

public class Strings {

  public static String ellipsize(String input, int maxCharacters) {
    if (maxCharacters < 3) {
      throw new IllegalArgumentException(
          "maxCharacters must be at least 3 because the ellipsis already take up 3 characters");
    }

    if (input == null || input.length() < maxCharacters) {
      return input;
    }

    return input.substring(0, maxCharacters - 3) + "...";
  }

  public static String abbreviate(String str, int maxWidth) {
    return abbreviate(str, 0, maxWidth);
  }

  public static String abbreviate(String str, int offset, int maxWidth) {
    if (str == null) {
      return null;
    }
    if (maxWidth < 4) {
      throw new IllegalArgumentException("Minimum abbreviation width is 4");
    }
    if (str.length() <= maxWidth) {
      return str;
    }
    if (offset > str.length()) {
      offset = str.length();
    }
    if ((str.length() - offset) < (maxWidth - 3)) {
      offset = str.length() - (maxWidth - 3);
    }
    if (offset <= 4) {
      return str.substring(0, maxWidth - 3) + "...";
    }
    if (maxWidth < 7) {
      throw new IllegalArgumentException("Minimum abbreviation width with offset is 7");
    }
    if ((offset + (maxWidth - 3)) < str.length()) {
      return "..." + abbreviate(str.substring(offset), maxWidth - 3);
    }
    return "..." + str.substring(str.length() - (maxWidth - 3));
  }

  private Strings() {
    throw new AssertionError("No instances of this class are allowed!");
  }
}
