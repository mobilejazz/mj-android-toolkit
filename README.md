MJ-Android-Toolkit
==================

A MobileJazz Android library project that eases certain common operations in Android apps.

Add it to your project
-------------------------------

### Repository

First, add the following to your app's `build.gradle` file:

```Gradle
repositories {
        maven { url 'https://oss.sonatype.org/content/repositories/snapshots' }
}
```

### Library

```gradle
dependencies {

    // ... other dependencies here

    compile 'com.mobilejazz.android-toolkit:toolkit-location:1.0.4-SNAPSHOT'
    compile 'com.mobilejazz.android-toolkit:library:1.0.4-SNAPSHOT'
}
```

License
-------

    Copyright 2015 MobileJazz

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.